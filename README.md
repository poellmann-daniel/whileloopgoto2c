# Convert WHILE/GOTO/LOOP programs to C

This 'transpiler' will convert WHILE/GOTO/LOOP programs to C source code.
It systematically replaces the keywords with the corresponding C syntax and adds some additional checks.

It is meant for demonstration and educational purposes only. 

# Limitations:
- It will NOT check for any syntactical errors and assumes that the source file is valid according to the syntax below
- The result of the subtraction operation cannot be negative. subtract(5, 7) = max(0, (5-7))  
- Variables can overflow. If this happens, the program execution stops.

# Syntax:
  where n of xn,Mn is a natural number; c is an integer constant
  
    WHILE xn =/= 0 DO <program> END
    LOOP xn DO <program> END
    IF xn = c THEN GOTO Mj
    <program> ; <program>
    xn := xm + c
    xn := xm - c
    Mn: <program>    ---    Markers can be placed at the beginning of lines, however they optional.
    # <Comment>
 
 When the program halts, it will output the value of the variable x0.
 You can mix WHILE, LOOP, GOTO as you like.

# Usage

Put your WHILE/GOTO/LOOP program code in a text file.
    
    ./wgl.sh <input-file>
    gcc <input-file>.c -o out
    ./out

See the `Makefile` for usage details.
    
The program works with the GNU version of `sed`. It has not been tested with other versions of sed (eg. BSD).

# Examples
See examples folder for code examples.
