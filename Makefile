PROGRAMS = subtract.goto add.loop multiply.while factorial.while fibonacci.loop negative.goto

compileAll:
	 for program in $(PROGRAMS); do \
	     bash wgl.sh examples/$$program && gcc -O3 examples/$$program.c -o $$program; \
	 done

run:
	for program in $(PROGRAMS); do \
	    ./$$program; \
	done

clean:
	for program in $(PROGRAMS); do \
	    rm $$program examples/$$program.c; \
	done
